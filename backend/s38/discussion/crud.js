let http = require('http');

// Mock Database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com",
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com",
	}
];

http.createServer(function (request, response){

	if(request.url == "/users" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'application/json'});

		response.write(JSON.stringify(directory));
		response.end();
	};

	if(request.url == "/users" && request.method == "POST"){

		// Declare and initialize a "requestBody" variable to empty string;
		// This will act as a placeholder for the data/resource that we do have.
		let requestBody = "";
			console.log(requestBody); // At this point, requestBody is still empty. 

		// Data is received from the client and is process in the "data" stream

		request.on('data', function(data){

			requestBody += data;
			console.log("This is from request.on(data)")
			console.log(requestBody)
		});

		// response "end" step - only runs after the request has completely been sent
		request.on('end', function(){

			console.log(requestBody);
			console.log(typeof requestBody);

			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			};

			console.log(newUser);

			directory.push(newUser);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		})
	}

}).listen(4000);

console.log('Server is running at localhost:4000!');