//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo(){

    return await (

       //add fetch here.
       
       fetch('<urlSample>')
       .then((response) => response.json())
       .then((json) => json)
   
   
   );

}



// Getting all to do list item
async function getAllToDo(){

   return await (

      fetch("https://jsonplaceholder.typicode.com/todos",{
         method: "GET",
         
         let array = 

      }).then(response => response.json())
      .then(json => return)



  );

}

// [Section] Getting a specific to do list item
async function getSpecificToDo(){
   
   return await (

       //Add fetch here.


   );

}

// [Section] Creating a to do list item using POST method
async function createToDo(){
   
   return await (

       //Add fetch here.


   );

}

// [Section] Updating a to do list item using PUT method
async function updateToDo(){
   
   return await (

       //Add fetch here.



   );

}

// [Section] Deleting a to do list item
async function deleteToDo(){
   
   return await (

       //Add fetch here.


   );

}




//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}