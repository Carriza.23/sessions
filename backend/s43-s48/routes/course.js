const express = require('express');
const courseController = require('../controllers/course');
const auth = require("../auth.js")
const router = express.Router();

// Destructuring of verify and verifyAdmin
const{verify, verifyAdmin} = auth;


// Create a course POST method

router.post("/", verify, verifyAdmin, courseController.addCourse)

// Get all courses
router.get("/all", courseController.getAllCourses )

// Get all active course
router.get("/", courseController.getAllActive);

// Get 1 specific course
router.get("/:courseId", courseController.getCourse);

// Updating a course (Admin only)
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);

// Enroll user


// [ SECTION ] Export Route System
module.exports = router;