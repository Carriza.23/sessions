console.log("Hellow World")

/* [ SECTION ] Syntax, Statements and Comments
JS Statements usually end with semicolon (;)*/

// Comments:
// There are two types of comments:
	// 1. The single-line comment denoted by //
	// 2. The multiple-line comment denoted by /**/,
	//double and asterisk in the middle

 // [ SECTION ] Variables
   // It is used to contain data
   // Any information that is used by an application is stored in what we call a "memory"

	// Declaring Variables

	// Declaring Variables - tells our devices that a variable name is created.

	let myVariableNumber;  /*A naming convension*/
	console.log(myVariableNumber);
	// let MyVariableNumber
	// let	my-variable-number
	// let my_variable_number

	/*Syntax
		let/const/var variableName*/

	// Declaring and Initializing Variables

	let productName = "desktop computer";
	console.log(productName);

	let productPrice = 18999
	console.log(productPrice)

	// const, impossible to reassign
	const interest = 3.539;

	// Reassigning variable values

	productName = "Laptop";
	console.log(productName);

	// interest = 4.239; Will return an error
	// console.log(interest);

	// Reassigning Variables vs Initializing Variables

	// Declare the variable

	myVariableNumber = 2023;
	console.log(myVariableNumber);

	// Multiple Variable Declarations

	let productCode = 'DC07';
	const productBrand = 'Deli';

	console.log(productCode,productBrand);

	// [ SECTION ] Data Types

		// Strings

		let country = "Philippines";
		let province = 'Metro Manila';

		// Concatenation Strings
		let fullAddress = province + ", " + country;
		console.log(fullAddress);

		let greeting = "I live in the " + country;
		console.log(greeting);

		let mailAddress = 'Metro Manila \n\nPhilippines';
		console.log(mailAddress);

		let message = "John's employees went home early";
		console.log(message);

		message = 'John\'s employees went home early again!';
		console.log(message);

		// Numbers
		let headcount = 26;
		console.log(headcount);

		// Decimal Numbers/Fraction
		let grade = 98.7;
		console.log(grade);

		// Exponential Notation
		let planetDistance = 2e10;
		console.log(planetDistance);

		// Combining text and strings
		console.log("John's garde last quarter is " + grade);

		// Boolean
		// Returns true or False

		let isMarried = false;
		let inGoodConduct = true;
		console.log("isMarried: " + isMarried);
		console.log("inGoodConduct: " + inGoodConduct);

		// Arrays
		let grades = [ 98.7, 92.1, 90.2, 94.6 ];
		console.log(grades);

		// Different data types

		let details = [ "John", "Smith", 32, true];
		console.log(details);
		console.log(details[0])
		console.log(details[1])
		console.log(details[2])
		console.log(details[3])

		// Objects
		// Composed of "key/value pair"

		let person = {
			fullName: "Juan Dela Cruz",
			age: 35,
			isMarried: false,
			contact: ["09123456782, 099531541"],
			address: {
				houseNumber: "345",
				city: "Manila"
			}
		};

		console.log(person);
		console.log(person.fullName);
		console.log(person.age);
		console.log(person.isMarried);
		// We only use [] to call the index number of the data from
		// the array
		console.log(person.contact);
		console.log(person.address.houseNumber);
		console.log(person.address);
		console.log(person.address.houseNumber);

		let arrays = [
			["hey", "hey1", "hey2"],
			["hey", "hey1", "hey2"],
			["hey", "hey1", "hey2"],
			];
		console.log(arrays);

		let myGrades = {
			firstGrading: 98.7,
			secondGrading:92.1,
			thirdGrading: 90.2,
			fourthGrading: 94.6
		};

		console.log(myGrades)

		// Typeof Operator
		// It will determine the type of the data.
		console.log(typeof myGrades);
		console.log(typeof arrays);
		console.log(typeof greeting);

		let spouse = null;
		let myNumber = 0;
		let myString = '';

		console.log(spouse);
		console.log(myNumber);
		console.log(myString);

		// Undefined
		let fullName;
		console.log(fullName)