// Array Mutator Methods

let fruits = ["Apple" , "Orange", "Kiwi", "Dragon Fruit"];

// push();
// Add element in an array

console.log("Current Array: ");
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log("Array after push method:");
console.log(fruits);


// Push multiple elements
fruits.push("Avocado","Guava");
console.log("Array after push method:");
console.log(fruits.length);
console.log(fruits);

function addFruit(fruit){
	// push parameter
	fruits.push(fruit);
	console.log(fruits);
}

addFruit("Pineapple");


// pop()
// 

fruits.pop();
fruits.pop();
console.log(fruits);
function removeFruit(){
	fruits.pop();
	console.log(fruits);
}

removeFruit();

// unshift()
// adds element in the beginning

fruits.unshift("Lime", "Banana");
console.log("Array after unshift method");
console.log(fruits);

function unshiftFruit(fruit) {
	fruits.unshift(fruit);
	console.log(fruits);
}

unshiftFruit("Calamansi");

// shift()
// removes element in the beginning

fruits.shift();
console.log("Array after shift method");
console.log(fruits);

function shiftFruit(fruit){
	fruits.shift();
	console.log(fruits);
}

shiftFruit();

// splice()
// simulataneously removes elements from a spliced index number and
// adds element
// syntax: array.splice(startIndex,deleteCount,elementsToBeAdded);

fruits.splice(1, 2, "Avocado");
console.log("Array after splice method");
console.log(fruits);

function spliceFruit(index,deleteCount,fruit){
	fruits.splice(index,deleteCount,fruit);
	console.log(fruits);
}
spliceFruit(1,1,"Cherry");
spliceFruit(2,1,"Durian");


// - Rearranges the array elements in alphanumeric order
// - Syntax
//    arrayName.sort();

    fruits.sort();
    console.log('Mutated array from sort method:');
    console.log(fruits);

// **Important Note:**
//     - The "sort" method is used for more complicated sorting functions.
//     - Focus the students on the basic usage of the sort method and discuss it's more advanced usage only when you are confident in the topic.
//     - Discussing this might confuse the students more given the amount of topics found in the session.

// reverse()

// - Reverses the order of array elements
// - Syntax
//     - arrayName.reverse();

    fruits.reverse();
    console.log('Mutated array from reverse method:');
    console.log(fruits);
