// [SECTION] - if, else if, else statement

let numA = -1;

// if
/*

Syntax:

if(condition){
	//code
}

*/

// true condition
if(numA < 5){
	console.log("Hello");
}

// false condition -> will not execute the code
if(numA < -5){
	console.log("Hello");
}

// String
let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York City!");
}

// else if

let numH = 1

if(numH < 0){
	console.log("Hello!");
}else if(numH > 0){
	console.log("World!");
}

city = "Tokyo"

if(city === "New York"){
	console.log("Welcome to New York City");
}else if(city === "Tokyo"){
	console.log("Welcome to Tokyo Japan");
}

// else statement

let num1 = 5
if(num1 === 0){
	console.log("Hello")
}else if(num1 === 3){
	console.log("World")
}else{
	console.log("Sorry all conditions are not met.")
}

// if, if else, else with function

function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return "Not a typhoon yet.";
	}else if(windspeed <= 61){
		return "Tropical Depression detected."
	}else if(windspeed >= 62 && windspeed <= 88){
		return "tropical storm detected."
	}else if (windspeed >= 89 && windspeed <= 117){
		return "Severe tropical storm detected";
	}else{
		return "Typhoon detected";
	}
}

let message = determineTyphoonIntensity(25);
message = determineTyphoonIntensity(85);
message = determineTyphoonIntensity(250);
console.log(message);

// Truthy

if(true){
	console.log("truthy");
}

if(1){
	console.log("truthy");
}

if([]){
	console.log("truthy");
}

// Falsy

if(0){
	console.log("Falsy")
}

if(undefined){
	console.log("Falsy")
}

// Single statement execution
// Ternary Statement
// let ternary = (1 < 18) ? "Yes" : "No";
// console.log(ternary);

let num2 = 10;

let ternary = (num2 > 18 || num2 === "10" ) ? "Yes" : "No";
console.log(ternary);

// Multiple Statement

let name;

function isOfLegalAge(){
	name= "John";
	return "You are of the legal age limit."
}

function isUnderAge(){
	name = "Jane";
	return "You are under the age limit"
}

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + "," + name);

// switch - case statement

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day){
	case "monday" :
		console.log("The color of the day is red.");
		break;
	case "tuesday" :
		console.log("The color of the day is orange");
		break;
	case "wednesday" :
		console.log("The color of the day is yellow");
		break;
	case "thursday" :
		console.log("The color of the day is green");
		break;
	case "friday" :
		console.log("The color of the day is blue");
		break;

	case "saturday" :
		console.log("The color of the day is indigo");
		break;
	case "sunday" :
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please enter a valid day!")
		break;
}