import { Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext.js';
import {useContext} from 'react'
import {Navigate} from 'react-router-dom'


export default function Profile(){

    const {user} = useContext(UserContext);

    return(
        (user.access === null) ?
            <Navigate to= "/courses"/>
        :
        <Row>
            <Col>

                <h1>Profile</h1>
                <h2>James Dela Cruz</h2>
                <hr/>
                <h4>Contacts</h4>
                <ul>
                    <li>Email: {user.email}</li>
                    <li>Mobile No. 09124753254</li>
                </ul>
            </Col>
          </Row>      




        )
}