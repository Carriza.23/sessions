import AppNavbar from './components/AppNavbar';
import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import CourseView from './pages/CourseView';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';
import Profile from './pages/Profile';
import AdminView from './pages/AdminView';

import {UserProvider} from './UserContext';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
//Bootstrap

// BrowserRouter, Routes, Route works together to setup components endpoints
import { BrowserRouter as Router } from 'react-router-dom';
import{ Route, Routes } from 'react-router-dom';
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import './App.css';

function App() {

  // Gets token from local storage and assigns the token to user state
  const[user, setUser] = useState({
    // token: localStorage.getItem('token')
    id: null,
    isAdmin: null

  });

  // const [Logged, setLogged] = useState({token: localStorage.getItem('Logged')});


  // unsetUser is a function for clearing local storage
  const unsetUser = () => {
    localStorage.clear();
  }
  // Check if user info is properly stored upon login and the local storage if cleared upon logout
  useEffect(() =>{
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    // The user state, setUser (setter function) and unsetUser function is provided in the UserContext to be shared to other pages/components.
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
      <Container fluid>
        <AppNavbar />
        <Routes>
            <Route path="/" element = {<Home />} />
            <Route path="/courses" element = {<Courses />} />
            <Route path="/register" element = {<Register />} />
            <Route path="/courses/:courseId" element = {<CourseView />} />
            <Route path="/adminView" element = {<AdminView />} />
            <Route path="/login" element = {<Login />} />
            <Route path="/logout" element = {<Logout />} />
            <Route path="/profile" element = {<Profile />} />
            < Route path="/*" element={<ErrorPage />} />
          </Routes>
      </Container>
      
    </Router>
    </UserProvider>
  );
}

export default App;
