function countLetter(letter, sentence) {
    let result = 0;
   

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
      if (letter.length !== 1) {
    return undefined; // Return undefined for invalid input
  }

  let count = 0;

  for (let i = 0; i < sentence.length; i++) {
    if (sentence[i] === letter) {
      count++;
    }
  }

  return count;
}

    



function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

     text = text.toLowerCase();

  // Step 2: Initialize an empty object to track encountered letters.
  const letterMap = {};

  // Step 3: Iterate through each character in the lowercased text.
  for (let i = 0; i < text.length; i++) {
    const char = text[i];

    // Step 4: Check if the character has been encountered before.
    if (letterMap[char]) {
      return false; // If it has, return false (not an isogram).
    }

    // Otherwise, mark it as encountered.
    letterMap[char] = true;
  }

  // Step 5: If no repeating letters were found, return true (isogram).
  return true;
}

function purchase(age, price) {


if (age < 13) {
        return undefined;
    }

    // Check if age is between 13 and 21 or age is 65 and above
    if ((age >= 13 && age <= 21) || age >= 65) {
        // Calculate a 20% discount
        let discountedPrice = price * 0.8;
        discountedPrice = discountedPrice * 100;
        // Round the discounted price
        let roundedDiscountedPrice = Math.round(discountedPrice);
        // Return the rounded discounted price as a string
        roundedDiscountedPrice = roundedDiscountedPrice / 100;
        return roundedDiscountedPrice.toString();
    }

    // Check if age is between 22 and 64
    if (age >= 22 && age <= 64) {
        // Return the rounded price as a string
        const noDiscount = Math.round(price * 100)/100.0;

        return noDiscount.toString();
    }
}

console.log(purchase(12, 2000));
console.log(purchase(20, 2000));
console.log(purchase(23, 2000));


function findHotCategories(items) {
    // Find categories that has no more stocks. The hot categories must be unique;
    // no repeating categories. The expected output after processing the items array
    // is ['toiletries', 'gadgets']. Only putting return ['toiletries', 'gadgets']
    // will not be counted as a passing test during manual checking of codes.

    const hotCategories = [];
    for (const item of items) {
        if (item.stocks === 0) {
            if (!hotCategories.includes(item.category)) {
                hotCategories.push(item.category)
            }
        }
    }

    return hotCategories;
}

const items = [
    {
        id: 'tltry001',
        name: 'soap',
        stocks: 14,
        category: 'toiletries'
    }, {
        id: 'tltry002',
        name: 'shampoo',
        stocks: 8,
        category: 'toiletries'
    }, {
        id: 'tltry003',
        name: 'tissues',
        stocks: 0,
        category: 'toiletries'
    }, {
        id: 'gdgt001',
        name: 'phone',
        stocks: 0,
        category: 'gadgets'
    }, {
        id: 'gdgt002',
        name: 'monitor',
        stocks: 0,
        category: 'gadgets'
    }
]

console.log(findHotCategories(items));

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.
    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes. 
    flyingVoters = [];
    for( const voter of candidateA){
        if(candidateB.includes(voter)){
            flyingVoters.push(voter);
        }
    }
    return flyingVoters;
}
   const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
   const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

   console.log(findFlyingVoters(candidateA, candidateB));


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};