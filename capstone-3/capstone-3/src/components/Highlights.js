import { Row, Col, Card } from 'react-bootstrap';

export default function Hightlights(){
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="card-body p-3">
			      <Card.Body >
			        <Card.Title>
			        	<h2>Custom Cakes</h2>
			        </Card.Title>
			        <Card.Text>
			          Where to get a custom cake? That is the question that SweetLife Cakes will answer. Check out the huge variety of custom cakes below and order today.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Greeting Cakes</h2>
			        </Card.Title>
			        <Card.Text>
			          Ready made cakes with your heartwarming greetings, available everyday to make your occassions sweeter.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Premium Cakes</h2>
			        </Card.Title>
			        <Card.Text>
			          Premium quality cakes, richer in flavour and taste.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}