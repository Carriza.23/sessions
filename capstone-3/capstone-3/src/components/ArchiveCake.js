import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCake({cake, isAvailable, fetchData}) {

    const archiveToggle = (cakeId) => {
        fetch(`https://capstone2-43ek.onrender.com/cake/archiveCake/${cakeId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Cake successfully disabled'
                })
                fetchData();

            }else {
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'Error',
                    text: 'Please Try again'
                })
                fetchData();
            }


        })
    }


        const activateToggle = (cakeId) => {
        fetch(`https://capstone2-43ek.onrender.com/cake/updateCakeAvailability/${cakeId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Cake successfully enabled'
                })
                fetchData();
            }else {
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'Error',
                    text: 'Please Try again'
                })
                fetchData();
            }


        })
    }
 

    return(
        <>
            {isAvailable ?

                <Button variant="danger" size="sm" onClick={() => archiveToggle(cake)}>Archive</Button>

                :

                <Button variant="success" size="sm" onClick={() => activateToggle(cake)}>Activate</Button>

            }
        </>

        )
}