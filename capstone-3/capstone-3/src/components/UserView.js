import React, { useState, useEffect } from 'react';
import CakeCard from './CakeCard';
import CakeSearch from './CakeSearch';


export default function UserView({cakeData}) {

    const [cake, setCake] = useState([])

    useEffect(() => {
        const cakeArr = cakeData.map(cake => {
            //only render the active courses
            if(cake.isAvailable === true) {
                return (
                    <CakeCard cakeProp={cake} key={cake._id}/>
                    )
            } else {
                return null;
            }
        })

        //set the courses state to the result of our map function, to bring our returned course component outside of the scope of our useEffect where our return statement below can see.
        setCake(cakeArr)

    }, [cakeData])

    return(
        <>
            <CakeSearch />
            { cake }
        </>
        )
}