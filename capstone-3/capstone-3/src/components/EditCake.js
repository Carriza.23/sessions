import {Button, Modal, Form} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';

export default function EditCake({cake}){

	// state for course id which will be used for fetching
	const [cakeId, setCakeId] = useState("");

	// state for editcourse modal
	const [showEdit, setShowEdit] = useState(false);

	// useState for our form (modal)
	const [cakeName, setCakeName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");

	// function for opening the edit modal

	const openEdit = (cakeId) => {
		setShowEdit(true);

		// to still get the actual data from the form
		fetch(`https://capstone2-43ek.onrender.com/cake/${cakeId}`)
		.then(res => res.json())
		.then(data => {
			setCakeId(data._id);
			setCakeName(data.cakeName);
			setDescription(data.description);
			setPrice(data.price)
		})
	}

	const closeEdit = () => {
		setShowEdit(false);
		setCakeName("");
		setDescription("");
		setPrice(0);
	}

	// function to save our update
	const editCake = (e, cakeId) => {
		e.preventDefault();

		fetch(`https://capstone2-43ek.onrender.com/cake/${cakeId}`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				cakeName: cakeName,
				description: description,
				price: price
			})
			})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Update Success!",
					icon: "success",
					text: "Cake Successfully Updated!"
				})

				closeEdit();
			}else{
				Swal.fire({
					title: "Update Error!",
					icon: "error",
					text: "Please try again!"
				})
				closeEdit();
			}

		})
	}

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => {openEdit(cake)}}>Edit</Button>

			 <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={e => editCake(e, cakeId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Cake</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>    
                        <Form.Group controlId="cakeName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" required value={cakeName} onChange={e => setCakeName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="cakeDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" required value={description} onChange={e => setDescription(e.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="cakePrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" required value={price} onChange={e => setPrice(e.target.value)}/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
		</>

		)
}