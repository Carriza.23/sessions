import React, { useState } from 'react';
import CakeCard from './CakeCard';
const CakeSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch('https://capstone2-43ek.onrender.com/cake/searchCake', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ cakeName: searchQuery })
      });
      const data = await response.json();
      setSearchResults(data);
      console.log(data);
    } catch (error) {
      console.error('Error searching for cake:', error);
    }
  };

  return (
    <div className='pt-5 container bg-p'>
      <h2>Cake Search</h2>
      <div className="form-group">
        <label htmlFor="cakeName">Cake Name:</label>
        <input
          type="text"
          id="cakeName"
          className="form-control"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
        />
      </div>
      <button className="btn btn-primary my-4" onClick={handleSearch}>
        Search
      </button>
      <h3>Search Results:</h3>
      <ul>
       
         { searchResults.map(cake => (
            <li key={cake._id}>
              <div className="p-3 bg-success">
                <CakeCard cakeProp={cake} />
              </div>
            </li>
          ))}
       
      </ul>
    </div>
  );
};

export default CakeSearch;