import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import navbar from '../styles/navbar.css';

import { Link, NavLink } from 'react-router-dom';

import {useState, useContext} from 'react';
import React, {Fragment} from 'react';
import UserContext from '../UserContext';


export default function AppNavbar(){


	const { user } = useContext(UserContext);

	// const [user, setUser] = useState(localStorage.getItem("token"));
	// console.log(user);

	return (
		<Navbar expand="lg" bg="light">
		      <Container>
		        <Navbar.Brand href="#home">SweetLife Cakes</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="me-auto">
		          {/* exact - useful for NavLinks to respond with exact path NOT on partial matches */}
		            <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
		            <Nav.Link as={NavLink} to="/cake" exact>Menu</Nav.Link>

		            {(user.id !== null)?
		            <>
		            <Nav.Link as={NavLink} to="/profile" exact>Profile</Nav.Link>
		            <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
		            </>
		            :
		            <React.Fragment>
			            <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
			            <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>

		            </React.Fragment>
		            }

		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
	)
}