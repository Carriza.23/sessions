import { Card, Button} from 'react-bootstrap';
// import coursesData from '../data/coursesData';
import {useState} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'
import '../styles/cakeCard.css';

export default function CakeCard({cakeProp}){
	// Checks to see if the data was successfully passed
	// console.log(props);
	// Every components recieves information in a form of an object
	// console.log(typeof props);

	const {_id, cakeName, description, price} = cakeProp;

	// Use the state hook in this component to be able to store its state

	/*
		Syntax:
			const [getter, setter] = useState(initialGetterValue);
	*/
	// const [count, setCount] = useState(0);

	// console.log(useState(0));

	// Function that keeps track of the enrollees for a course
	//  function enroll() {
  //   if (count < 30) {
  //     setCount(count + 1);
  //     console.log('Enrollees: ' + count);
  //   } else {      
  //     window.alert('Maximum enrollments reached (30).');
  //   }
  // }

	return (
		
				<Card className="custom-card">
			      <Card.Body>
			        <Card.Title className="card-title">{cakeName}</Card.Title>
			        <Card.Subtitle>Description:</Card.Subtitle>
					 <Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					  <Card.Text>PhP {price}</Card.Text>				
					<Link className="btn btn-primary" to={`/cake/${_id}`}>Details</Link>		        
			      </Card.Body>			      
			    </Card>
	)
}

// Check if the CourseCard component is getting the correct prop types

CakeCard.propTypes = {
		cakeProp : PropTypes.shape({
		cakeName: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}