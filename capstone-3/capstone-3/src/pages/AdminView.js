import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';

import EditCake from '../components/EditCake';
import ArchiveCake from '../components/ArchiveCake';


export default function AdminView({ cakeData, fetchData }) {


    const [cake, setCake] = useState([])


    //Getting the coursesData from the courses page
    useEffect(() => {
        const cakeArr = cakeData.map(cake => {
            return (
                <tr key={cake._id}>
                    <td>{cake._id}</td>
                    <td>{cake.cakeName}</td>
                    <td>{cake.description}</td>
                    <td>{cake.price}</td>
                    <td className={cake.isAvailable ? "text-success" : "text-danger"}>
                        {cake.isAvailable ? "Available" : "Unavailable"}
                    </td>
                    <td> <EditCake cake={cake._id} fetchData={fetchData} /> </td> 
                    <td><ArchiveCake cake={cake._id} isActive={cake.isActive} fetchData={fetchData}/></td>
                </tr>
                )
        })

        setCake(cakeArr)

    }, [cakeData])


    return(
        <>
            <h1 className="text-center my-4"> Admin Dashboard</h1>
            
            <Table striped bordered hover responsive>
                <thead>
                    <tr className="text-center">
                        <th>ID</th>
                        <th>Cake Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th colSpan="2">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    {cake}
                </tbody>
            </Table>    
        </>

        )
}