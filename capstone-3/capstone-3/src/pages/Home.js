import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import home from '../styles/home.css';
import cakeImage from './customCake.jpg';
import unicorn from './unicorn.jpg';
import jungleCake from './jungleCake.jpg';
import babyShark from './babyShark.jpg';



export default function Home() {
  const data = {
    title: "SweetLife Cakes",
    content: "Celebrations made better and sweeter.",
    destination: "/cake",
    label: "Order Now!"
  }

  return (
    <div className="home-background">
      <Banner data={data} />
      <Highlights />
      <div className="container-fluid">
        <img src={cakeImage} alt="cake" className="w-100" style={{ visibility: 'hidden' }} />
      </div>
    </div>
  );
}