// [ SECTION ] Dependecies and Modules
const mongoose = require("mongoose");

// [ SECTION ] Schema/Blueprint
const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "userId ID is required"],
  },
  products: [
    {
      productId: {
        type: String,
        required: [true, "Product ID is required"],
      },
      quantity: {
        type: Number,
        required: [true, "Quantity is required"],
      },
    },
  ],
  totalAmount: {
    type: Number,
  },
  orderDate: {
    type: Date,
    default: new Date(),
  },
  deliveryDate: {
    type: Date,
  },
  status: {
    type: String,
    default: "Order on process",
  },
});

// [ SECTION ] Model
module.exports = mongoose.model("Order", orderSchema);
