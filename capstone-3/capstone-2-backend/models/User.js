//[SECTION] Modules and Dependencies
const mongoose = require("mongoose");

//[SECTION] Schema/Blueprint
const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : Number, 
        required : [true, "Mobile No is required"]
    },
    address : {
    	type: String,
    	required : [true, "Address is required"]
    },
    order : [
   {
        cakeName : {
            type: String,
            required : [true, "cakeName is required"]
        },
        description : {
            type : String,
            required : [true, "description is required"]
        },
        quantity : {
            type : Number,
            required: [true, "quantity is required"]
        }
    }]
})

//[SECTION] Model
module.exports = mongoose.model("User", userSchema);