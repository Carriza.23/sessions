const express = require("express");
const userController = require("../controllers/user-controller");
const auth = require("../auth.js");
const router = express.Router();
const { verify, verifyAdmin } = auth;

// Checking existing email
router.post("/checkEmail", (req, res) => {
  userController
    .checkEmailExist(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Registration
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// user authentication
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// user authentication
// router.get("/get-all-user", (req, res) => {
// 	userController.getAllUser(req.body).then(resultFromController => res.send(resultFromController));
// });

// retrieve user details
router.get("/get/details", verify, userController.getProfile);

// [ SECTION ] Export Route System
module.exports = router;
