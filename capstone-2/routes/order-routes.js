const express = require('express');
const orderController = require('../controllers/order-controller');
const auth = require("../auth.js")
const router = express.Router();
const {verify, verifyAdmin} = auth;
const jwt = require('jsonwebtoken');


// create order
router.post("/newOrder", verify, orderController.newOrder);




// [ SECTION ] Export Route System
module.exports = router;
