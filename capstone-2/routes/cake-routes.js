const express = require('express');
const cakeController = require('../controllers/cake-controller');
const auth = require("../auth.js")
const router = express.Router();

// Destructuring of verify and verifyAdmin
const{verify, verifyAdmin} = auth;

// Add cake
router.post("/", verify, verifyAdmin, cakeController.addCake)


// Get all cakes
router.get("/all", cakeController.getAllCake );

// Get single cake
router.post("/get/cakeName", cakeController.getSingleCake);

// Get available cake
router.get("/available", cakeController.getAvailableCake);

// Update cake
router.put("/updateCake/:cakeId",verify, verifyAdmin, cakeController.updateCake);

// Update/ Archive cake
router.put("/archiveCake/:cakeId",verify, verifyAdmin, cakeController.archiveCake);

// Update/ Availability of cake
router.put("/updateCakeAvailability/:cakeId",verify, verifyAdmin, cakeController.updateCakeAvailability);


router.post("/searchCake", cakeController.searchCakeByName);

// [ SECTION ] Export Route System
module.exports = router;