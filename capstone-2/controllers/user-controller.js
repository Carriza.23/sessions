// [ SECTION ] Dependecies and Modules
const User = require("../models/User.js");
const bcrypt = require("bcrypt");

const Cake = require("../models/Cake.js");
const Order = require("../models/Order.js");

const auth = require("../auth");

// check email exists
/*
	Steps:
		1. Use mongoose 'find' method to find duplicate emails
		2. Use the 'then' method to send a response back to the frontend application based on the result of the "find" method.
*/
module.exports.checkEmailExist = (reqBody) => {
  console.log(reqBody);
  console.log(reqBody.email);
  return User.find({ email: reqBody.email }).then((result) => {
    console.log(result);
    if (result.length > 0) {
      return true;
    } else {
      return false;
    }
  });
};

// registration
module.exports.registerUser = async (reqBody) => {
  try {
    const newUser = new User({
      firstName: reqBody.firstName,
      lastName: reqBody.lastName,
      email: reqBody.email,
      mobileNo: reqBody.mobileNo,
      password: bcrypt.hashSync(reqBody.password, 10),
      address: reqBody.address,
    });

    console.log(reqBody);
    const user = await newUser.save();
    return user ? true : false;
  } catch (error) {
    console.error(error);
    return false;
  }
};

// user authentication
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    console.log(result);
    if (result === null) {
      //return res.send(false); // the email doesn't exist in our DB
      return false; // the email doesn't exist in our DB
    } else {
      //const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );

      console.log(isPasswordCorrect);

      if (isPasswordCorrect) {
        //return res.send({ access: auth.createAccessToken(result)})
        return { access: auth.createAccessToken(result) };
      } else {
        return false; // Passwords do not match
      }
    }
  });
  // .catch(err => res.send(err))
};

// Retrieve user details

module.exports.getProfile = async (req, res) => {
  return User.findById(req.user.id).then(result => {
    result.password = "";
    return res.send(result);
  })
  .catch(error => error)
};
