// [ SECTION ] Dependecies and Modules
const Cake = require('../models/Cake.js');
const bcrypt = require('bcrypt');

const auth = require('../auth.js');

// adding cake
module.exports.addCake = (req, res) => {
	
	let newCake= new Cake({
		cakeName: req.body.cakeName,
		description: req.body.description,
		price: req.body.price	
	});

	return newCake.save().then((cake, error) => {
		if(error){
			return res.send (false);
		} else {
			return res.send(true);
		}
	})
	.catch(error => res.send(error));
};

// retrieving all cakes
module.exports.getAllCake = (req,res) => {
	return Cake.find({}).then(result => {
		if(result.length === 0){
			return res.send("No cake in the database.");
		}else{
			return res.send(result);
		}
	})
}

// getting single cake
// module.exports.getSingleCake = (req, res) =>{
// 	return Cake.findById(req.params.cakeId).then(result =>{
// 		if(result === 0){
// 			return res.send("Cannot find cake with the provided ID")
// 		}else{

// 			if(result.isActive === false){
// 				return res.send("The cake you are trying to order is not available.")
// 			}else{
// 				return res.send(result);
// 			}
			
// 		}
// 	})
// 	.catch(error => res.send("Please enter a correct cake ID"))
// }

// search cake
module.exports.getSingleCake = (req, res) => {
  const { cakeName } = req.body; // Extract the cakeName from the request body

  Cake.findOne({ cakeName: cakeName }) // Use findOne to find a single cake by name
    .then(result => {
      if (!result) {
        return res.status(404).send(false);
      } else if (!result.isAvailable) {
        return res.status(400).send("The cake you are trying to order is not available.");
      } else {
        return res.send(result);
      }
    })
    .catch(error => {
      console.error(error);
      return res.status(500).send("Internal server error");
    });
};


// getting available
module.exports.getAvailableCake = (req, res)=>{
	return Cake.find({isAvailable : true}).then(result =>{
		if(result.length === 0){
			return res.send("Sorry, There is no available cakes right now")
		}else{
			return res.send(result);
		}
	})
}



// updating cake price
module.exports.updateCake = (req, res) => {
    const updatedCake = {
        cakeName: req.body.cakeName,
        price: req.body.price,
        description: req.body.description
    };

    Cake.findByIdAndUpdate(req.params.cakeId, updatedCake)
        .then((cake) => {
            if (!cake) {
                return res.status(404).send(false); // Handle the case when the cake is not found
            }
            return res.send(true);
        })
        .catch((error) => {
            console.error(error);
            return res.status(500).send("Error updating cake price"); // Handle other errors
        });
};

// archive a cake
module.exports.archiveCake = (req, res) => {
    const updatedCake = {
        isAvailable: false
    };

    Cake.findByIdAndUpdate(req.params.cakeId, updatedCake)
        .then((cake) => {
            if (!cake) {
                return res.status(404).send("Cake not found");
            }
            return res.status(200).send("Successfully archived the cake");
        })
        .catch((error) => {
            console.error(error);
            return res.status(500).send("Error archiving the cake");
        });
};

// update cake availability
module.exports.updateCakeAvailability = (req, res) => {
    const updatedCakeAvailability = {
        isAvailable: true
    };

    Cake.findByIdAndUpdate(req.params.cakeId, updatedCakeAvailability)
        .then((cake) => {
            if (!cake) {
                return res.status(404).send("Cake not found");
            }
            return res.status(200).send("Successfully updated cake availability");
        })
        .catch((error) => {
            console.error(error);
            return res.status(500).send("Error updating the cake");
        });
};

module.exports.searchCakeByName = async (req, res) => {
    try {
      const { cakeName } = req.body;
  
      // Use a regular expression to perform a case-insensitive search
      const cake = await Cake.find({
        cakeName: { $regex: cakeName, $options: 'i' }
      });
  
      res.json(cake);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
};