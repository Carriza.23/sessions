// [ SECTION ] Dependecies and Modules
const Order = require("../models/Order.js");
const { v4: uuidv4 } = require("uuid");
const auth = require("../auth");

// create new order
// module.exports.newOrder = async (req, res) => {
// 	if(req.user.isAdmin){
// 		return res.send("You cannot do this action.")
// 	}

// }
// 	let newOrder = {
// 		cakeName : req.body.cakeName,
// 		price : req.body.price
// 	}

// module.exports.newOrder = async (req, res) => {
// 	console.log(req.user.id);
// 	console.log(req.body.cakeName;

// 	if(req.user.isAdmin){
// 		return res.send("You cannot do this action.")
// 	}

// 	let isOrderUpdated = await User.findById(req.user.id).then(user => {
// 		let newOrder = {
// 			cakeName: req.body.cakeName,

// 		}

// 		let listOfOrders = user.order;

// 		let filteredCakeId = [];

// 		listOfOrders.forEach((result) => {
// 			filteredCakeId.push(result.cakeId);
// 		});

// 		console.log(filteredCakeId);

// 		let orderDuplicate = filteredCakeId.includes(req.body.cakeId);

// 		console.log("order duplicate: " + orderDuplicate);

// 		if(orderDuplicate){
// 			return res.send("You already ordered this product.");
// 		}

// 			user.enrollments.push(newEnrollment);

// 		return user.save().then(user => true).catch(error => res.send(error));

// 	})

// 	if(isUserUpdated !== true){
// 		return res.send({message: isUserUpdated})
// 	}

// 	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
// 		let enrollee = {
// 			userId: req.user.id
// 		}

// 		course.enrollees.push(enrollee);

// 		return course.save().then(course => true).catch(error => res.send(error));
// 	})

// 	if(isCourseUpdated !== true){
// 		return res.send({message: isCourseUpdated});
// 	}

// 	if(isUserUpdated && isCourseUpdated){
// 		return res.send("You are now enrolled to a course. Thank you!");
// 	}
// }

// module.exports.getEnrollments = (req, res) => {
// 	User.findById(req.user.id).then(result => {
// 		if(result.enrollments == [] || result.enrollments == null){
// 			return res.send("You are not enrolled to any courses.")
// 		}else{
// 			res.send(result.enrollments);
// 		}
// 	}).catch(error => res.send(error));
// }

// adding cake
module.exports.newOrder = async (req, res) => {
  const newOrder = new Order({
    userId: req.user.id,
    cakeName: req.body.cakeName,
    price: req.body.price,
    productId: req.body.productId,
    quantity: req.body.quantity,
  });

  newOrder
    .save()
    .then((order) => {
      return res.send("Your order is being processed. Thank you!");
    })

    .catch((error) => {
      console.error(error);
      return res.status(500).send("Error creating the order");
    });
};


// adding orders
module.exports.getOrders= async (req, res) => {
    if(req.user.isAdmin){
        return res.send("You cannot do this action.")
    }else{
        let getOrders = Order.findById(req.user.id).then(result =>{
            return res.send(result.order)
        
        })

    }
}