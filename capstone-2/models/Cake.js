// [ SECTION ] Dependecies and Modules
const mongoose = require('mongoose');

// [ SECTION ] Schema/Blueprint
const cakeSchema = new mongoose.Schema({
	cakeName : {
		type: String,
		required: [ true, 'Cake is required!' ]
	},
	description : {
		type: String,
		required: [ true, 'Description is required!' ]
	},
	price : {
		type: Number,
		required: [ true, 'Price is required!' ]
	},
	isAvailable : {
		type: Boolean,
		default: true
	}
	
});

// [ SECTION ] Model
module.exports = mongoose.model('Cake', cakeSchema);